const express = require('express'); // require da lib express

const v1About = require('./V1/about'); //caminho que o servido irá fazer
const v1Home = require('./V1/home');
const v1Troubleshooting = require('./V1/troubleshooting');

module.exports = (app) => { //importação e exportação
    const routerV1 = express.Router(); //função Router serve para distribuição das rotas;
    v1Home(routerV1); //v1Home - que irá para servidor Home - é um servidor;
    v1About(routerV1);
    v1Troubleshooting(routerV1);

    app.use('/', routerV1); //rota principal
}