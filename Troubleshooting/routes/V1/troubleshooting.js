const troubleshootingController = require('../../Controllers/troubleshootingController');
const middlewareValidaDTO = require("../../Ultis/date-Validate");

module.exports = (routeV1) => {

    routeV1.route('/troubleshooting').get(troubleshootingController.getTroubleshooting);
    routeV1.route('/troubleshooting').post(middlewareValidaDTO("body", troubleshootingController.postTroubleshootingSchema), troubleshootingController.postTroubleshooting);

}