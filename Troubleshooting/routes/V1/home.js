const homeController = require('../../Controllers/homeController');

module.exports = (routeV1) => {

    routeV1.route('/').get(homeController.getHome);
        
}