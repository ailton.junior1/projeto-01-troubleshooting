const aboutController = require('../../Controllers/aboutController');

module.exports = (routeV1) =>{
    routeV1.route('/about').get(aboutController.getAbout);
}