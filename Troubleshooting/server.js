const express = require('express'); // requisitar a lib server;
const server = express(); // converter para uma função para execução;

const cors = require('cors');
server.use(cors());

const bp = require('body-parser'); // requisitar a lib body parser (que serve para codificar em json os dados do body)

server.use(bp.json()); // codificador do body parser (documentação)
server.use(bp.urlencoded()); // codificador do body parser (documentação)

server.set('view engine', 'ejs'); // engine para execução do EJS (padrão)

const path = require('path'); // require do "path" - serve para unificar caminhos e procurar extensões;
server.use(express.static(path.join(__dirname, 'public'))); // serve para mostrar o caminho de todos os itens estáticos para
//conversão em EJS -

server.set('views', path.join(__dirname, 'views')); // require dos arquivos EJS para exibição (primeiro 'views' nome criado);
//__dirname (rota padrão para JS)

const routes = require('./routes/routes'); // caminho para distrubuir as rotas;
routes(server); //server irá importar as rotas;

const door = 3030; // porta de execução

server.listen(door, ()=> { 
    console.log("server is running on " + door);
});