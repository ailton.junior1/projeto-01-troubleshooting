async function postData(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
      method: 'POST',
      mode: 'cors', 
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
      },
      redirect: 'follow',
      referrerPolicy: 'no-referrer',
      body: JSON.stringify(data),
    });
      return response.json();
  }

  postData('https://api.voucherjet.com/api/v1/p/generator', {  "count": 10, "pattern": "SER######", "characters" : "0123456789" })
  .then(data => {
    var serialNumber = document.getElementById('button-form');
    console.log(data.codes[1]);
    serialNumber.value = data.codes[1]
  });
  
 