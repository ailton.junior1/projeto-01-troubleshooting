var options =  {
    onKeyPress: function(cep, e, field, options) {
      var masks = ['00000-000', '0-00-00-00'];
      var mask = (cep.length>7) ? masks[1] : masks[0];
      $('.crazy_cep').mask(mask, options);
  }};
  
  $('.crazy_cep').mask('00000-000', options);