/* const troubleshootingController = require('../Controllers/troubleshootingController') */

module.exports = (contexto, schema) => {

    return async (req, res, next) => {

        console.log("@@@@@@@@@@");
        console.log(req[contexto]);

       const resultado = schema.validate(req[contexto], { abortEarly: false }); 
 
        
        if (resultado.error) {
                        
            console.log(resultado.error );

            return res.status(400).send(resultado.error);

        }

        next();
    } 

}