const machineOptionDB = require('../Models/machineOptions');
const ufOptionDB = require('../Models/ufOptions');
const descriptionMachineOptionDB = require('../Models/descriptionMachineOptions');
const evidanceOptionDB = require('../Models/evidanceOption');
const mapperOption = require('../Ultis/Mappers/select-options');
const Joi = require('joi');
const path = require('path');
const fs = require('fs');
const ejs = require('ejs');
const htmlToPdf = require('html-pdf-node');

const getTroubleshooting = (req, res, next) => {
    const viewModel = { 
        machineModel: mapperOption('model', 'id', machineOptionDB.machineModelList()),
        descriptionMachine: mapperOption('description', 'id', descriptionMachineOptionDB.machineDescriptionList()),
        evidanceService: mapperOption('evidance', 'id', evidanceOptionDB.evidanceList()),
        ufOption: mapperOption('uf', 'id', ufOptionDB.ufOptionList()),
    }
    res.render('troubleshooting.ejs', viewModel);    
}


const postTroubleshootingSchema = Joi.object({

    numberOS: Joi.string().required(),

    machineNumber: Joi.number().required(),

    breakdownList: Joi.string().required(),

    kindType: Joi.string().required(),

    machineType: Joi.string().required(),

    selectEvidance: Joi.number().required(),

    serialNumber: Joi.string().required(),

    clientCEP: Joi.string().required(),
    
    AddressFinal: Joi.string().required(),

    dateFinal: Joi.date().required(),

    CustomerName: Joi.string().required(), 

    spareParts: Joi.string().required(),

    UFState: Joi.string().required(),

    reasonContact: Joi.string().required(),
 
    descriptionRelatory: Joi.string().min(20).required(),

    cnpjFinal: Joi.number().min(1).required()
})

// modal

const postTroubleshooting = (req, res, next) => {
   
    const { numberOS, dateFinal, machineNumber, machineType, reasonContact, breakdownList, selectEvidance, kindType, 
            serialNumber, descriptionRelatory, clientCEP, AddressFinal, cnpjFinal, CustomerName, spareParts, UFState } = req.body;  
    const machineTypeSelected = descriptionMachineOptionDB.searchById(machineType);
    const selectEvidanceSelected = evidanceOptionDB.searchById(selectEvidance);
    const UFStateSelected = ufOptionDB.searchById(UFState);
    const machineNumberSelected = machineOptionDB.searchById(machineNumber);

    const pdfViewModel = {numberOS, dateFinal, machineNumber: machineNumberSelected.model, machineType: machineTypeSelected.description, reasonContact, breakdownList, selectEvidance: selectEvidanceSelected.evidance, kindType, 
        serialNumber, descriptionRelatory, clientCEP, AddressFinal, cnpjFinal, CustomerName, spareParts, UFState: UFStateSelected.uf}

//html

    const filePath = path.join(__dirname, "../views/troubleshooting-pdf.ejs");
    const templateHtml = fs.readFileSync(filePath, 'utf-8');

//montar o pdf

    const htmlPronto = ejs.render(templateHtml, pdfViewModel);

//retornar o pdf

const file = {
    content: htmlPronto  
  };

  const configuracoes = {
    format: 'A4',
    printBackground: true
  };
  
  htmlToPdf.generatePdf(file, configuracoes)
  .then((resultPromessa) => {
      res.contentType("application/pdf");
      res.send(resultPromessa);
    });

}

module.exports = {
    getTroubleshooting, 
    postTroubleshootingSchema,
    postTroubleshooting,
}