const getAbout = (req, res, next) => {
    res.render('about.ejs'); 
}

module.exports = {
    getAbout,
}