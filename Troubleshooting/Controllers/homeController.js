const getHome = (req, res, next) => {
    res.render('index.ejs');
}

module.exports = {
    getHome,
}