const ufOption = [
    {id: 1, uf: "AC"},
    {id: 2, uf: "AL"},
    {id: 3, uf: "AP"},
    {id: 4, uf: "AM"},
    {id: 5, uf: "BA"},
    {id: 6, uf: "CE"},
    {id: 7, uf: "DF"},
    {id: 8, uf: "ES"},
    {id: 9, uf: "GO"},
    {id: 10, uf: "MA"},
    {id: 11, uf: "MT"},
    {id: 12, uf: "MS"},
    {id: 13, uf: "MG"},
    {id: 14, uf: "PA"},
    {id: 15, uf: "PB"},
    {id: 17, uf: "PR"},
    {id: 18, uf: "PE"},
    {id: 19, uf: "PI"},
    {id: 20, uf: "RJ"},
    {id: 21, uf: "RN"},
    {id: 22, uf: "RS"},
    {id: 23, uf: "RO"},
    {id: 24, uf: "RR"},
    {id: 25, uf: "SC"},
    {id: 26, uf: "SP"},
    {id: 27, uf: "SE"},
    {id: 29, uf: "TO"},
]

const ufOptionList = () => {
    return ufOption;
}

const searchById = (id) => {
    const result = ufOptionList().find((item)=>{
        return parseInt(item.id) === parseInt(id);
    })

    return result;
}

const searchByUf = (uf) => {
    const result = ufOptionList().find((item)=> {
        return item.uf === uf;
    })
}

module.exports = {
    searchById,
    searchByUf,
    ufOptionList
}