const machineModel = [ //array de objetos usado para filtrar dados;
    {
        id: 1,
        model: "GA",
    },

    {
        id: 2,
        model: "ZR / ZT / ZA / ZB",
    },

    {
        id: 3,
        model: "XA",
    },

    {
        id: 4,
        model: "L",
    },

    {
        id: 5,
        model: "Vacuum Pump"
    },

]

const machineModelList = () => { //função necessária para retornar os valores
    return machineModel
}

const searchById = (id) => {
    const result = machineModel.find((item)=>{
        return parseInt(item.id) == parseInt(id);

        // filter = lista / find = um objeto/item
    })

return result;

}

const searchByModel = (model) => {
    const result = machineModel.find((item)=>{
        return item.model == model;
    })

    return result;
}


module.exports = {
    searchByModel,
    searchById,
    machineModelList,
}

