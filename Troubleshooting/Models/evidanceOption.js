const evidanceService = [
    {
        id: 1,
        evidance: 'Vibration',
    },

    {
        id: 2,
        evidance: 'High Temperature',
    },

    {
        id: 3,
        evidance: 'Water in Outlet',
    },

    {   
        id: 4,
        evidance: 'Oil in Outlet'

    },

    {
        id: 5,
        evidance: 'Docked Element',
    },

    {
        id: 6,
        evidance: 'Broken Gearbox',
    },

    {
        id: 7,
        evidance: 'Broken Gearbox',
    },

    {
        id: 8,
        evidance: 'Filters and carter',

    },
]

const evidanceList = () => {
    return evidanceService;
};

const searchById = (id) => {
    const result  = evidanceList().find((item) => {
        return parseInt(item.id) == parseInt(id)
    })
    return result;
}

const searchByEvidance = (evidance) => {
    const result = evidanceList().find((item) => {
        return item.evidance == evidance;
})
    return result;
}

module.exports = {
    evidanceList,
    searchById,
    searchByEvidance
}