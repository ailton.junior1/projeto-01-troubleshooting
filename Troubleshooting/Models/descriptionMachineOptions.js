const machineDescription = [
    {      
        id: 1,
        description: 'Screw Free Oil Compressor'
    },
    
    {
        id: 2,
        description: "Portatil Compressor",
    },

    {
        id: 3,
        description: "Piston Compressor"
    },

    {
        id: 4,
        description: "Vacuum Machine",
    },

]

const machineDescriptionList = () => {
    return machineDescription
};

const searchById = (id) => {
    const result = machineDescriptionList().find((item)=>{
        return parseInt(item.id) == parseInt(id);

    })

return result;

}

const searchByDescription = (description) => {
    const result = machineDescription.find((item)=> {
        return item.description == description;
    })
    return result;
}

module.exports = {
    machineDescriptionList,
    searchByDescription,
    searchById, 
}